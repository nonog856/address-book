package book;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class AddressBook {

    HashMap<String, String> contacts = new HashMap<>();

    String inputFileName = "src/input.csv";
    String outputFileName = "src/output.csv";

    public AddressBook () {
        this.loadContactFile();
    }

    public void loadContactFile() {
        BufferedReader bufferedReader;
        try {
            bufferedReader = new BufferedReader(new FileReader(inputFileName));
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                // System.out.println(line);
                this.addContact(line);
            }
            bufferedReader.close();
        } catch (IOException e) {
            System.out.println("IOException catched while reading: " + e.getMessage());
        }
    }

    public void showAddressBook () {
        System.out.println("Agenda ----------------------------");

        for (Map.Entry<String, String> entry : contacts.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();

            System.out.println(key + " <-> " + value);
        }
    }

    public void addContact (String contact) {
        String[] phoneName = contact.split(",");
        contacts.put(phoneName[0], phoneName[1]);
    }

    public void updateContactsFile () {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(outputFileName));
            String phoneName;

            for (Map.Entry<String, String> contact : contacts.entrySet()) {
                String key = contact.getKey();
                String value = contact.getValue();

                phoneName = key + "," + value;

                bufferedWriter.write(phoneName);
                bufferedWriter.newLine();
            }

            bufferedWriter.close();
        } catch (IOException e) {
            System.out.println("IOException catched while writing: " + e.getMessage());
        }
    }
}
