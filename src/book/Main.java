package book;

public class Main {

    public static void main(String[] args) {

        AddressBook addressBook = new AddressBook();

        addressBook.showAddressBook();
        addressBook.addContact("+52 225 353 6741,Silvia Tello");

        addressBook.showAddressBook();
        addressBook.updateContactsFile();
    }
}
